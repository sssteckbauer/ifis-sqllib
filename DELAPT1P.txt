  DELETE
    FROM FISDBC1.APT_APRVL_TRAN_V APT
   WHERE APT.DOC_TYP   = 'INV'
     AND SUBSTR(APT.USER_ID,1,3)
                     NOT IN ('CSH', 'GAO', 'BUR', 'MTM', 'MSV', 'SCI')
     AND SUBSTR(APT.APV_TPLT_CD,1,1)
                     NOT IN ('P','E','M','R','H','F')
     AND NOT EXISTS
          (SELECT FK_APL_USER_ID
             FROM FISDBC1.APH_APRVL_HDR_V APH
            WHERE APH.FK_APL_SEQ_NBR       = 3
              AND APH.FK_APL_USER_ID       = APT.USER_ID
              AND APH.FK_APL_APV_TPLT_CD   = APT.APV_TPLT_CD
              AND APH.IDX_UNAPRVD_SETF     = 'Y')
