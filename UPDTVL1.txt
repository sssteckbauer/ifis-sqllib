       UPDATE FISDBC1.TVL_TRVL_V TVL                                    00026000
           SET  EMPLY_IND = 'N'                                         00027000
          WHERE TVL.IREF_ID IN                                          00028000
             (SELECT PRS.IREF_ID                                        00029000
               FROM FISDBC1.PRS_PRSN_V PRS                              00030000
               WHERE   EMP_STATUS  = ' ')                               00031000
           AND  TVL.ENTPSN_IND = 'P'                                    00032000
           AND  TVL.EMPLY_IND =  'E'                                    00033000
