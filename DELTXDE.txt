  DELETE
    FROM FISDBE1.TXD_TXT_DTL_V
   WHERE FK_TXH_DOC_TYP_SEQ   =  7
    AND (FK_TXH_TXT_ENTYCD LIKE 'NT%'
      OR FK_TXH_TXT_ENTYCD LIKE 'NI%'
      OR FK_TXH_TXT_ENTYCD LIKE 'NE%')
