       UPDATE FISDBC1.TVL_TRVL_V TVL                                    00040027
           SET  EMPLY_IND = 'E'                                         00050027
          WHERE TVL.IREF_ID IN                                          00070027
             (SELECT PRS.IREF_ID                                        00080027
               FROM FISDBC1.PRS_PRSN_V PRS                              00081027
                   WHERE  EMP_ID     <> ' '                             00082027
                   AND EMP_STATUS = 'A')                                00100027
           AND  TVL.ENTPSN_IND = 'P'                                    00110027
           AND  TVL.EMPLY_IND <> 'E';                                   00120027
