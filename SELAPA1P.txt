  SELECT APA.FK_APL_USER_ID
        ,' '
        ,APA.FK_APL_APV_TPLT_CD
        ,' '
        ,CHAR(APA.FK_APL_LVL_SEQ_NBR)
        ,' '
        ,APA.ALTRNT_APRVL_CD
    FROM FISDBC1.APA_APRVL_ALT_V  APA
   WHERE APA.FK_APL_SEQ_NBR              =  3
     AND SUBSTR(APA.FK_APL_USER_ID,1,3)
                     NOT IN ('CSH', 'GAO', 'BUR', 'MTM', 'MSV', 'SCI')
     AND SUBSTR(APA.FK_APL_APV_TPLT_CD,1,1)
                     NOT IN ('P','E','M','R','H','F')
     AND NOT EXISTS
        (SELECT 1
           FROM FISDBC1.APH_APRVL_HDR_V APH
          WHERE APH.FK_APL_SEQ_NBR       = APA.FK_APL_SEQ_NBR
            AND APH.FK_APL_USER_ID       = APA.FK_APL_USER_ID
            AND APH.FK_APL_APV_TPLT_CD   = APA.FK_APL_APV_TPLT_CD
            AND APH.IDX_UNAPRVD_SETF     = 'Y')
  ORDER BY
         APA.FK_APL_USER_ID
        ,APA.FK_APL_APV_TPLT_CD
        ,APA.FK_APL_LVL_SEQ_NBR
